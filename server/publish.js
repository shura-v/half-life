SECTIONS_COLLECTION = new Meteor.Collection('sections');
DATA_COLLECTION = new Meteor.Collection('data');

Sortable.collections.push('sections');
//
//SECTIONS_COLLECTION.allow({
//    update: function (userId) {
//        console.log(arguments)
//        return Meteor.userId() === userId;
//    },
//    fetch: ['order', 'name']
//});

Meteor.publish('sections', function () {
    return SECTIONS_COLLECTION.find({
        userId: this.userId
    });
});

Meteor.publish('data', function () {
    return DATA_COLLECTION.find({userId: this.userId});
});
//
//var sections = SECTIONS_COLLECTION.find({}).fetch();
//
//sections.forEach(function (section, i) {
//    SECTIONS_COLLECTION.update(section._id, {$set: {order: i}})
//});
//
//console.log(SECTIONS_COLLECTION.find({}).fetch())