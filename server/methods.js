Meteor.methods({
    sectionCreate: function (data) {
        return SECTIONS_COLLECTION.insert({
            userId: Meteor.userId(),
            name: data.name,
            unit: data.unit,
            halfLife: data.halfLife,
            archived: false,
            lastModified: null,
            lastModifiedData: null
        });
    },
    sectionUpdate: function (data) {
        SECTIONS_COLLECTION.update(data.id, {
            $set: {
                userId: Meteor.userId(),
                name: data.name,
                halfLife: data.halfLife,
                lastModified: new Date().getTime()
            }
        });

        return data.id;
    },
    entryAdd: function (data) {
        var existing = DATA_COLLECTION.findOne({
            userId: Meteor.userId(),
            datetime: data.datetime,
            sectionId: data.sectionId
        });

        SECTIONS_COLLECTION.update(data.sectionId, {
            $set: {
                lastModifiedData: new Date().getTime()
            }
        });

        if (existing) {
            DATA_COLLECTION.update(existing._id, {
                $inc: {value: data.value}
            });
            return existing._id;
        } else {
            return DATA_COLLECTION.insert({
                userId: Meteor.userId(),
                value: data.value,
                datetime: data.datetime,
                sectionId: data.sectionId
            });
        }
    },
    entryDelete: function (id) {
        var section = DATA_COLLECTION.findOne(id);
        var sectionId = section.sectionId;

        if (sectionId === undefined) {
            return;
        }

        SECTIONS_COLLECTION.update(sectionId, {
            $set: {
                lastModifiedData: new Date().getTime()
            }
        });

        DATA_COLLECTION.remove(id);
    },
    sectionDelete: function (id) {
        DATA_COLLECTION.remove({
            sectionId: id,
            userId: Meteor.userId() // in case of sloppy edit
        });

        SECTIONS_COLLECTION.remove(id);

        return id;
    },
    sectionArchive: function (id) {
        SECTIONS_COLLECTION.update(id, {
            $set: {
                archived: true,
                lastModified: new Date().getTime()
            }
        });

        return id;
    },
    sectionUnarchive: function (id) {
        SECTIONS_COLLECTION.update(id, {
            $set: {
                archived: false,
                lastModified: new Date().getTime()
            }
        });
        return id;
    }
});
