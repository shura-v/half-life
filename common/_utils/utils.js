UTILS = {};

UTILS.eval = function (variable) {
    if (variable instanceof ReactiveVar) {
        return variable.get();
    }

    return variable;
};
