var humanizeDuration = require('humanize-duration');

TIME = {};

TIME.SECOND_MS = 1000;
TIME.MINUTE_MS = TIME.SECOND_MS * 60;
TIME.HOUR_MS = TIME.MINUTE_MS * 60;
TIME.DAY_MS = TIME.HOUR_MS * 24;

TIME.inputDatetimeFormat = 'YYYY-MM-DD[T]HH:mm';

TIME.now = function () {
    return Date.now();
};

TIME.clearTime = function (date) {
    var d = new Date(date);
    d.setHours(0);
    d.setMinutes(0);
    d.setSeconds(0);
    d.setMilliseconds(0);
    return d.getTime();
};

if (Meteor.isClient) {
    TIME.shortHumanizer = humanizeDuration.humanizer({
        language: "shortEn",
        round: true,
        languages: {
            shortEn: {
                y: function () {
                    return "y";
                },
                mo: function () {
                    return "mo";
                },
                w: function () {
                    return "w";
                },
                d: function () {
                    return "d";
                },
                h: function () {
                    return "h";
                },
                m: function () {
                    return "m";
                },
                s: function () {
                    return "s";
                },
                ms: function () {
                    return "ms";
                }
            }
        }
    });
}