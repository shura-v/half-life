MATH = {};

MATH.valueFromLimit = function (limit, halfLife, frequency) {
    if (frequency == null) {
        frequency = TIME.DAY_MS;
    }

    return limit * (1 - Math.pow(0.5, frequency / halfLife));
};

MATH.accumulated = function (lastValue, timeElapsed, halfLife) {
    return lastValue * Math.pow(0.5, timeElapsed / halfLife);
};

MATH.limitFromValue = function (value, halfLife, frequency) {
    if (frequency == null) {
      frequency = TIME.DAY_MS;
    }

    return value / (1 - Math.pow(0.5, frequency / halfLife));
};

MATH.getMlCountForDose = function (dose, tabletDose, waterVolume) {
    return dose / (tabletDose / waterVolume);
};

MATH.getDoseFromMl = function (ml, tabletDose, waterVolume) {
    return ml * (tabletDose / waterVolume);
};
