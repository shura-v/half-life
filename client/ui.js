var humanizeDuration = require('humanize-duration');

UI.registerHelper('capitalize', function (str) {
    str = UTILS.eval(str);
    return str[0].toUpperCase() + str.slice(1);
});

UI.registerHelper('notInfinity', function (number) {
    return number !== Infinity;
});

UI.registerHelper('toFixed', function (number, decimal) {
    return number.toFixed(decimal);
});

/**
 * @param {Number} date
 * @returns {String}
 */
UI.registerHelper('formatDate', function (date) {
    date = UTILS.eval(date);
    return moment(date).format('YYYY-MM-DD HH:mm');
});

UI.registerHelper('debug', function () {
    console.log(this, arguments);
});

UI.registerHelper('formatHalfLife', function (time) {
    time = UTILS.eval(time);
    return moment.duration(time).asHours() + ' hours';
});

UI.registerHelper('humanizeDuration', function (duration) {
    duration = UTILS.eval(duration);
    return humanizeDuration(duration, {round: true});
});

UI.registerHelper('eval', function (value) {
    return UTILS.eval(value);
});
