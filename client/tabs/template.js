Template.tabs.helpers({
    sections: function () {
        return SECTIONS_COLLECTION.find({
            archived: {
                $ne: true
            }
        });
    },
    className: function (activeId) {
        var classNames = [];

        if (activeId === this._id) {
            classNames.push('active');
        }

        return classNames.join(' ');
    },
    sortableOptions: function () {
        return {
            sort: true
        };
    }
});
