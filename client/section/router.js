Router.route('/section', function () {
    this.redirect('/section/add');
}, {
    name: 'section'
});

Router.route('/section/:id', function () {
    var dbEntry = SECTIONS_COLLECTION.findOne({_id: this.params.id});

    if (dbEntry) {
        var model = new SectionModel(dbEntry);
        var viewModel = new SectionViewModel(model);

        this.render('tabs', {to: 'top', data: dbEntry});
        this.render('section', {to: 'content', data: viewModel});
    } else {
        dbEntry = SECTIONS_COLLECTION.findOne({});

        if (dbEntry) {
            this.redirect('/section/' + dbEntry._id);
        } else {
            this.redirect('/section/add');
        }
    }
}, {
    waitOn: function () {
        return Meteor.subscribe('sections');
    },
    name: 'section-id'
});
