var humanizeDuration = require('humanize-duration');

Template.sectionInfo.helpers({
    accumulated: function () {
        return this.accumulatedValue.get();
    },
    daily: function () {
        return this.periodicValue.get();
    },
    dailyFromLast: function () {
        return this.periodicFromLastValue.get();
    },
    durationFromLastDose: function () {
        return humanizeDuration(this.timeFromLastDose.get(), {round: true});
    },
    mean: function () {
        return this.meanValue.get();
    },
    durationFromLastDoseInHalfLives: function () {
        return this.timeFromLastDoseInHalfLives.get();
    }
});
