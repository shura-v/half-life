Router.route('/section/add', function () {
    this.render('tabs', {to: 'top', data: "add"});
    this.render('sectionAdd', {to: 'content'});
}, {
    waitOn: function () {
        return Meteor.subscribe('sections');
    },
    name: 'add'
});
