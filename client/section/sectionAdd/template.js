Template.sectionAdd.events({
    'submit form': function (e) {
        e.preventDefault();
        var data = $(e.target).serializeJSON();
        data.halfLife *= 60 * 60 * 1000;
        Meteor.call('sectionCreate', data, function (error, _id) {
            Router.go('/section/' + _id);
        });
        e.target.reset();
    }
});
