SectionModel = function (params) {
    this.id = params._id;
    this.archived = params.archived;
    this.halfLife = params.halfLife;
    this.lastModified = new Date(params.lastModified);
    this.lastModifiedData = new Date(params.lastModifiedData);
    this.order = params.order;
    this.name = params.name;
    this.unit = params.unit;
    this.userId = params.userId;
    this.data = this.loadData();
};

SectionModel.prototype.loadData = function () {
    return DATA_COLLECTION.find({
        sectionId: this.id,
        userId: Meteor.userId()
    }, {
        sort: {
            datetime: 1
        }
    }).fetch();
};

SectionModel.prototype.slice = function (data, minDate, maxDate) {
    if (!data.length || data[0].datetime > maxDate || data[data.length - 1].datetime < minDate) {
        return [];
    }

    var first = data[0];
    var last = data[data.length - 1];
    var i = 0;

    while (minDate >= data[i].datetime && i < data.length) {
        first = data[i++];
    }

    i = data.length - 1;
    while (maxDate <= data[i].datetime && i > 0) {
        last = data[i--];
    }

    var indexStart = data.indexOf(first);
    var indexEnd = data.indexOf(last);

    return data.slice(indexStart, indexEnd + 1);
};

