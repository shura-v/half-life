Router.route('/section/:id/edit', function () {
    var dbSection = SECTIONS_COLLECTION.findOne({_id: this.params.id});

    if (dbSection) {
        this.render('tabs', {to: 'top', data: dbSection});
        this.render('sectionEdit', {to: 'content', data: new SectionModel(dbSection)});
    } else {
        this.redirect('/section/add');
    }
}, {
    waitOn: function () {
        return Meteor.subscribe('sections');
    },
    name: 'section-edit'
});
