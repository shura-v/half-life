Template.sectionEdit.helpers({
    toHours: function (value) {
        return value / 60 / 60 / 1000;
    }
});

Template.sectionEdit.events({
    'submit form': function (e) {
        e.preventDefault();
        var data = $(e.target).serializeJSON();
        data.halfLife *= 60 * 60 * 1000;
        Meteor.call('sectionUpdate', data);
        e.target.reset();
        Router.go('/section/' + data.id);
    }
});
