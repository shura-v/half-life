Template.sectionData.helpers({
    expanded: function () {
        return Session.get('section-table-expanded');
    }
});

Template.sectionData.events({
    'click .toggle': function () {
        return Session.set('section-table-expanded', !Session.get('section-table-expanded'));
    }
});
