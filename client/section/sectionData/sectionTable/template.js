Template.sectionTable.events({
    'click .delete': function (e) {
        e.preventDefault();
        Meteor.call('entryDelete', this._id);
    }
});

Template.sectionTable.helpers({
    entries: function () {
        return this.data.slice().reverse().slice(0, 50);
    }
});
