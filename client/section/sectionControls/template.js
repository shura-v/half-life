Template.sectionControls.events({
    'click .delete': function (e) {
        e.preventDefault();
        if (confirm('Sure?')) {
            Meteor.call('sectionDelete', this.id);
        }
    },
    'click .archive': function (e) {
        e.preventDefault();
        Meteor.call('sectionArchive', this.id, function () {
            Router.go('/archive');
        });
    }
});
