Template.sectionEntry.events({
    'submit form': function (e) {
        e.preventDefault();

        if (e.target.datetime.value === "") {
            var d = moment().startOf('minute');
            $(e.target.datetime).val(d.format(TIME.inputDatetimeFormat));
        }

        var data = $(e.target).serializeJSON();
        data.value = Number(data.value);
        data.datetime = moment(data.datetime).toDate().getTime();

        Meteor.call('entryAdd', data);
    },
    'click .date-now': function (e) {
        e.preventDefault();

        var form = $(e.target).closest('form');
        var d = moment().local().startOf('minute');
        form.find('[name=datetime]').val(d.format(TIME.inputDatetimeFormat));
    },
    'click .date-today': function (e) {
        e.preventDefault();

        var form = $(e.target).closest('form');
        var d = moment().local().startOf('day');
        form.find('[name=datetime]').val(d.format(TIME.inputDatetimeFormat));
    }
});
