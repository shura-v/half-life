SectionViewModel = function (model) {
    this.model = model;

    this.data = undefined;

    this.lastNonZeroEntry = new ReactiveVar();
    this.accumulatedValue = new ReactiveVar(0);
    this.periodicValue = new ReactiveVar(0);
    this.periodicFromLastValue = new ReactiveVar(0);
    this.timeFromLastDoseInHalfLives = new ReactiveVar(0);
    this.timeFromLastDose = new ReactiveVar(0);
    this.meanValue = new ReactiveVar(0);

    this.intervals = {};

    this.halfLivesCount = 5;
    this.updateInterval = 16 * 20;

    this.updateDataFromModel();
    this.startHeartBeat();
};

Object.defineProperties(SectionViewModel.prototype, {
    id: {
        get: function () {
            return this.model.id;
        }
    },
    halfLife: {
        get: function () {
            return this.model.halfLife;
        }
    }
});

SectionViewModel.prototype.destroy = function () {
    // console.log('SectionViewModel.prototype.destroy');
};

SectionViewModel.prototype.updateDataFromModel = function () {
    this.data = this.model.data.map(function (current, i, array) {
        if (i === 0) {
            current.accumulated = current.value;
            return current;
        }

        var prev = array[i - 1];

        current.accumulated = current.value + MATH.accumulated(prev.accumulated, current.datetime - prev.datetime, this.model.halfLife);
        return current;
    }, this);

    this.data.sort(function (a, b) {
        return a.datetime - b.datetime;
    });

    if (this.data.length === 0) {
        return;
    }

    var lastNonZeroEntry = this.getLastNonZeroEntryFromNow();
    var valueFromLimit = MATH.valueFromLimit(lastNonZeroEntry.accumulated, this.model.halfLife);

    this.lastNonZeroEntry.set(lastNonZeroEntry);
    this.periodicFromLastValue.set(valueFromLimit);
};

SectionViewModel.prototype.startHeartBeat = function () {
    if (this.intervals[this.model.id]) {
        Meteor.clearInterval(this.intervals[this.model.id]);
        delete this.intervals[this.model.id];
    }

    this.intervals[this.model.id] = Meteor.setInterval(this.updateCurrentValues.bind(this), this.updateInterval);
};

SectionViewModel.prototype.updateCurrentValues = function () {
    var lastNonZeroEntry = this.lastNonZeroEntry.get();

    if (!lastNonZeroEntry) {
        return;
    }

    var timeFromLastDose = TIME.now() - lastNonZeroEntry.datetime;
    var accumulatedValue = MATH.accumulated(lastNonZeroEntry.accumulated, timeFromLastDose, this.model.halfLife);

    //this.setPeriodicValue();
    this.timeFromLastDose.set(timeFromLastDose);
    this.accumulatedValue.set(accumulatedValue);
    this.timeFromLastDoseInHalfLives.set(timeFromLastDose / this.halfLife);
};

SectionViewModel.prototype.getLastNonZeroEntryFromNow = function () {
    var now = TIME.now();

    var foundEntry = this.data.slice().reverse().find(function (entry) {
        if (entry.datetime < now && entry.value) {
            return true;
        }
    });

    return foundEntry || this.data[this.data.length - 1];
};

SectionViewModel.prototype.getInterpolatedData = function (options) {
    var min = options.min;
    var max = options.max;
    var step = options.step;

    var shiftAndPush = function () {
        var anchor = data.shift();
        array.push([anchor.datetime, anchor.accumulated]);
        date = anchor.datetime;
        return anchor;
    };

    if (!step) {
        step = 60 * 1000;
    }

    var data = this.data.slice();
    var date = data[0].datetime;
    var currentAnchor;
    var array = [];

    while (data.length > 1 && data[1].datetime < min) {
        currentAnchor = shiftAndPush();
    }

    while (date <= max) {
        if (data.length && (currentAnchor === undefined || date >= data[0].datetime)) {
            currentAnchor = shiftAndPush();
            date += step;
            continue;
        }

        if (date <= max && date !== currentAnchor.datetime && date >= min) {
            array.push([date, MATH.accumulated(currentAnchor.accumulated, date - currentAnchor.datetime, this.model.halfLife)]);
        }

        date += step;
    }

    while (data.length) {
        currentAnchor = shiftAndPush();
    }

    return array;
};

SectionViewModel.prototype.getMean = function (min, max) {
    var data = this.reduceDaily(min, max);
    if (!data.length) {
        return 0;
    }
    var total = data.reduce(function (prev, current) {
        return prev + current.value;
    }, 0);
    var days = (max - min) / TIME.DAY_MS;
    return total / days;
};

SectionViewModel.prototype.updateMean = function (min, max) {
    var mean = this.getMean(min, max);
    this.meanValue.set(mean);
};

SectionViewModel.prototype.setPeriodicValue = function (accumulated) {
    if (accumulated === undefined) {
        accumulated = this.accumulatedValue.get();
    }

    var value = MATH.valueFromLimit(accumulated, this.model.halfLife);

    this.periodicValue.set(value);
};

SectionViewModel.prototype.getNavigatorData = function () {
    var data = this.data.map(function (entry) {
        return [entry.datetime, entry.accumulated];
    });

    data.push([this.getMaxFutureDate(), null]);

    return data;
};

SectionViewModel.prototype.getMaxFutureDate = function () {
    var extrapolateTime = this.model.halfLife * this.halfLivesCount;

    return Math.max(
        this.lastNonZeroEntry.get().datetime + extrapolateTime,
        TIME.now() + extrapolateTime
    );
};

SectionViewModel.prototype.getTableData = function () {
    return this.model.data.map(function (entry) {
        return [entry.datetime, entry.value];
    });
};

SectionViewModel.prototype.reduceDaily = function (min, max) {
    return this.data.reduce(function (prev, current) {
        if (min != null && current.datetime < min || max != null && current.datetime > max) {
            return prev;
        }

        if (prev.length === 0) {
            return [{
                datetime: TIME.clearTime(current.datetime),
                value: current.value,
                accumulated: current.accumulated
            }]
        }

        var p = prev[prev.length - 1];
        p.accumulated = current.accumulated;

        if (TIME.clearTime(p.datetime) === TIME.clearTime(current.datetime)) {
            p.value += current.value;
            return prev;
        }

        prev.push({
            datetime: TIME.clearTime(current.datetime),
            value: current.value,
            accumulated: current.accumulated
        });

        return prev;
    }.bind(this), []);
};

SectionViewModel.prototype.getFirstEntryDatetime = function () {
    return this.data[0].datetime;
};

SectionViewModel.prototype.getClosestEntry = function (time) {
    var i = this.data.length;
    while (i--) {
        if (this.data[i].datetime < time && this.data[i].value) {
            return this.data[i];
        }
    }
};
