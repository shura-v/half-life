var Highstock = require('highcharts/highstock');
var moment = require('moment');

SectionChartView = function ($container) {
    this.$container = $container;
    this.viewModel = undefined;
    this.chart = undefined;

    this.mouseOnChart = false;
};

SectionChartView.prototype.setViewModel = function (viewModel) {
    this.viewModel = viewModel;

    if (!this.chart) {
        this.createChart();
        this.setInitialExtremes();
        this.bindEvents();
    }

    this.update();
};

SectionChartView.prototype.createChart = function () {
    this.chart = new Highstock.stockChart({
        chart: {
            renderTo: this.$container[0],
            zoomType: 'x'
        },
        xAxis: {
            events: {
                setExtremes: this.onAfterSetExtremes.bind(this)
            },
            minRange: 60 * 1000
        },
        yAxis: {
            min: 0
        },
        tooltip: {
            pointFormat: '<span style="color:{point.color}">\u25CF</span>{series.name}: <b>{point.y:.8f} mg</b><br/>'
        },
        rangeSelector: {
            selected: 0
        },
        series: this.getSeriesArrayData(),
        time: {
            useUTC: false
        }
    })
};

SectionChartView.prototype.bindEvents = function () {
    this.$container.on('mouseenter', this.handleMouseEnter.bind(this));
    this.$container.on('mouseleave', this.handleMouseLeave.bind(this));
    this.$container.on('mouseleave', this.removeHorizontalLine.bind(this));
    this.$container.on('mousemove', this.drawHorizontalLine.bind(this));
};

SectionChartView.prototype.unbindEvents = function () {
    this.$container.off('mouseenter');
    this.$container.off('mouseleave');
    this.$container.off('mousemove');
};

SectionChartView.prototype.handleMouseEnter = function () {
    this.mouseOnChart = true;
};

SectionChartView.prototype.handleMouseLeave = function () {
    this.mouseOnChart = false;
};

SectionChartView.prototype.removeHorizontalLine = function () {
    this.chart.yAxis[0].removePlotLine('horizontal');
    this.updatePeriodicValue();
};

SectionChartView.prototype.drawHorizontalLine = function (e) {
    e = this.chart.pointer.normalize(e);

    var axis = this.chart.yAxis[0];
    var value = axis.toValue(e.chartY);

    axis.removePlotLine('horizontal');
    axis.addPlotLine({
        id: 'horizontal',
        color: '#999',
        dashStyle: 'shortdot',
        width: 1,
        value: value
    });

    this.updatePeriodicValue(value);
};

SectionChartView.prototype.update = function () {
    this.getSeriesArrayData().forEach(function (obj, i) {
        this.chart.series[i].setData(obj.data, true, false, false);
    }, this);

    this.onAfterSetExtremes();
};

SectionChartView.prototype.drawNowLine = function () {
    var axis = this.chart.xAxis[0];

    if (!this.chart) {
        return;
    }

    axis.removePlotLine('now');
    axis.addPlotLine({
        id: 'now',
        color: 'red',
        dashStyle: 'line',
        width: 1,
        value: TIME.now(),
        label: this.viewModel.accumulatedValue.get()
    });
};

SectionChartView.prototype.setInitialExtremes = function () {
    var start = moment(this.viewModel.lastNonZeroEntry.get().datetime).add(-1, 'month').toDate().getTime();
    var end = moment(this.viewModel.getMaxFutureDate()).toDate().getTime();

    this.chart.xAxis[0].setExtremes(start, end);
    this.viewModel.updateMean();
};

SectionChartView.prototype.pointFormatter = function (point) {
    var time = point.x;
    var value = point.y;
    var closestEntry = this.viewModel.getClosestEntry(time);
    var html = 'Accumulated: <b>' + value.toFixed(3) + ' mg</b><br/>';
    if (closestEntry !== undefined) {
        var timeFromPreviousDose = (time - closestEntry.datetime);
        var halfLives = timeFromPreviousDose / this.viewModel.halfLife;
        html += 'As a daily dose: <b>' + MATH.valueFromLimit(value, this.viewModel.halfLife).toFixed(3) + ' mg</b><br/>';
        html += '<b>Since previous dose:</b><br/>';
        html  += 'Time: <b>' + TIME.shortHumanizer(timeFromPreviousDose, {round: true}) + '</b><br/>';
        if (halfLives !== Infinity) {
            html += 'Time in half-lives: <b>' + halfLives.toFixed(3) + '</b><br/>';
        }
    }
    return html;
};

SectionChartView.prototype.getSeriesArrayData = function () {
    var navigatorData = this.viewModel.getNavigatorData();
    var tableData = this.viewModel.getTableData();
    var self = this;

    return [
        {
            data: navigatorData,
            type: 'line',
            marker: {
                enabled: true,
                radius: 2
            },
            lineWidth: 0,
            name: 'Accumulated',
            dataGrouping: {
                enabled: false
            },
            tooltip: {
                pointFormatter: function () {
                    return self.pointFormatter(this);
                }
            }
        },
        {
            data: navigatorData,
            type: 'line',
            lineWidth: 1,
            radius: 2,
            name: 'Intermediate',
            dataGrouping: {
                enabled: false
            },
            tooltip: {
                pointFormatter: function () {
                    return self.pointFormatter(this);
                }
            }
        },
        {
            data: tableData,
            name: 'Daily sum',
            type: 'area',
            dataGrouping: {
                enabled: true,
                approximation: 'sum',
                units: [
                    ['day', [1, 7, 30]]
                ],
                forced: true
            }
        }
    ];
};

SectionChartView.prototype.updatePeriodicValue = function (value) {
    if (this.mouseOnChart && value === undefined) {
        return;
    }

    if (value === undefined) {
        this.viewModel.setPeriodicValue();
        return;
    }

    var extremes = this.chart.yAxis[0].getExtremes();

    if (value >= extremes.min && value <= extremes.max) {
        this.viewModel.setPeriodicValue(value);
    } else {
        this.viewModel.setPeriodicValue();
    }
};

SectionChartView.prototype.getCurrentOptions = function () {
    var axis = this.chart.xAxis[0];
    return {
        min: axis.min,
        max: axis.max,
        step: Math.ceil((axis.max - axis.min) / axis.width / 2)
    }
};

SectionChartView.prototype.onAfterSetExtremes = function () {
    var options = this.getCurrentOptions();
    var extremes = this.chart.xAxis[0].getExtremes();
    this.viewModel.updateMean(extremes.min, Math.min(Date.now(), extremes.max));
    var interpolatedData = this.viewModel.getInterpolatedData(options);
    this.chart.series[1].setData(interpolatedData, true, false, false);
};
