Template.sectionChart.onRendered(function() {
    var $container = Template.instance().$('.highstock');

    var chart = new SectionChartView($container);

    this.autorun(function () {
        chart.setViewModel(Template.currentData());
    });

    this.autorun(function () {
        chart.drawNowLine();
        chart.updatePeriodicValue();
    }.bind(this));
});

Template.sectionChart.onDestroyed(function () {
    var viewModel = Template.currentData();
    viewModel.destroy();
});
