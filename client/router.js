Router.configure({
    layoutTemplate: 'template'
});

Router.route('/', function () {
    var sections = SECTIONS_COLLECTION.find({
        archived: {
            $ne: true
        }
    }).fetch();

    if (sections.length) {
        this.redirect('/section/' + sections[0]._id);
    } else {
        this.redirect('/section/add');
    }
}, {
    waitOn: function () {
        return Meteor.subscribe('sections');
    },
    name: 'root'
});
