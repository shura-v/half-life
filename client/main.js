SECTIONS_COLLECTION = new Meteor.Collection('sections');
DATA_COLLECTION = new Meteor.Collection('data');

var HighStock = require('highcharts/highstock');

Meteor.subscribe('sections');
Meteor.subscribe('data');

Meteor.startup(function () {
  HighStock.setOptions({
    time: {
        useUTC: false
    }
  });
});
