Client = function Client () {
    this.name = 'Half Life';
    this.intervals = {};
    this.chart = undefined;
    this.$container = undefined;
    this.model = undefined;
    this.templateData = undefined;
    this.mouseOnChart = false;
    this.datetimeFormat = 'YYYY-MM-DD[T]HH:mm';
    this.bindEvents();
};

