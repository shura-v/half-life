Template.archive.helpers({
    sections: function () {
        return SECTIONS_COLLECTION.find({
            archived: true
        });
    },
    empty: function () {
        return SECTIONS_COLLECTION.find({
                archived: true
            }).count() === 0;
    }
});

Template.archive.events({
    'click .unarchive': function (e) {
        e.preventDefault();
        Meteor.call('sectionUnarchive', this._id, function (error, id) {
            Router.go('/section/' + id);
        });
    },
    'click .delete': function (e) {
        e.preventDefault();
        if (confirm('Sure?')) {
            Meteor.call('sectionDelete', this._id);
        }
    }
});
