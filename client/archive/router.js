Router.route(
    '/archive',
    function () {
        this.render(null, {to: 'top'});
        this.render('archive', {to: 'content'});
    },
    {
        waitOn: function () {
            return Meteor.subscribe('sections');
        },
        name: 'archive'
    }
);
